﻿$(function () {

    var NewsDotNet = {};

    NewsDotNet.GridManager = {};
    //************************* ARTICLES GRID
    NewsDotNet.GridManager.articlesGrid = function (gridName, pagerName) {

        var colNames = [
                'Id',
                'Заголовок',
                'Тело статьи',
                'Теги',
                'Состояние',
                'Адрес',
                'Адрес изображения',
                'Дата создания',
                'Дата последнего изменения'
        ];

        var columns = [];

        columns.push({
            name: 'Id',
            hidden: true,
            key: true
        });
        
        columns.push({
            name: 'Title',
            index: 'Title',
            width: 250,
            formatter: function (cellvalue, options, rowObject) {
                return '<a href="/Articles/' + rowObject.AddressName + '"target="_blank">' + cellvalue + '</a>';
            }
        });

        columns.push({
            name: 'Body',
            index: 'Body',
            width: 250,
            sortable: false,
            hidden: true
        });

        columns.push({
            name: 'Tags',
            width: 150,
            sortable: false
        });

        columns.push({
            name: 'State',
            index: 'State',
            width: 100,
            align: 'center',
            formatter: function (cellvalue, options, rowObject) {
                if (cellvalue == 0)
                    return "Черновик"
                else if (cellvalue == 1)
                    return "Опубликованная"
                else if (cellvalue == 2) return "Удаленная";
            }
        });

        columns.push({
            name: 'AddressName',
            width: 200,
            sortable: false
        });
        
        columns.push({
            name: 'TitleImagePath',
            width: 200,
            sortable: false
        });

        columns.push({
            name: 'CreatedTime',
            index: 'CreatedTime',
            width: 150,
            align: 'center',
            sorttype: 'date',
            datefmt: 'm/d/Y'
        });

        columns.push({
            name: 'LastChangedTime',
            index: 'LastChangedTime',
            width: 160,
            align: 'center',
            sorttype: 'date',
            datefmt: 'm/d/Y'
        });

        $(gridName).jqGrid({
            url: '/AdminHome/Articles',
            datatype: 'json',
            mtype: 'GET',
            height: 'auto',
            toppager: true,

            colNames: colNames,
            colModel: columns,

            pager: pagerName,
            rownumbers: true,
            rownumWidth: 40,
            rowNum: 10,
            rowList: [10, 20, 30],

            sortname: 'CreatedTime',
            sortorder: 'desc',
            viewrecords: true,

            jsonReader: {
                repeatitems: false
            },

            afterInsertRow: function (rowid, rowdata, rowelem) {
                
                var published = rowdata["State"] == 1;

                if (!published) {
                    $(gridName).setRowData(rowid, [], {
                        color: '#9D9687'
                    });
                    $(gridName + " tr#" + rowid + " a").css({
                        color: '#9D9687'
                    });
                }

                var tags = rowdata["Tags"];
                var tagStr = "";

                $.each(tags, function (i, t) {
                    if (tagStr) tagStr += ", "
                    tagStr += t.Name;
                });


                $(gridName).setRowData(rowid, { "Tags": tagStr });
            }
        });

        var deleteOptions = {
            url: '/AdminHome/DeleteArticle',
            caption: 'Удалить статью',
            processData: "Сохранение...",
            msg: "Удалить статью?",
            closeOnEscape: true,
            afterSubmit: NewsDotNet.GridManager.afterSubmitHandler
        };

        var btnAddProperties = {
            caption: "",
            buttonicon:"ui-icon-plus",
            title: "Новая запись",
            onClickButton: function(){location="/Admin/AdminArticles/New";},
            position:"first"
        }

        var btnEditProperties = {
            caption: "",
            buttonicon:"ui-icon-pencil",
            title: "Редактировать выбранную запись",
            onClickButton: function() {
                var selectedRow = $(gridName).jqGrid('getGridParam', 'selrow');
                if (!selectedRow)
                {
                    $( "#dialog" ).dialog();
                    return;
                }
                var addrName = $(gridName).jqGrid('getCell', selectedRow, 'AddressName');
                location = "/Admin/AdminArticles/Edit/" + addrName;
            },
            position:"first"
        }

        $(gridName).navGrid(pagerName, { cloneToTop: true, search: false, edit: false, add: false }, {}, {}, deleteOptions)
                    .navButtonAdd(gridName + "_toppager", btnEditProperties)
                    .navButtonAdd(pagerName, btnEditProperties)
                    .navButtonAdd(gridName + "_toppager", btnAddProperties)
                    .navButtonAdd(pagerName, btnAddProperties);
    };
    //************************* TAGS GRID
    NewsDotNet.GridManager.tagsGrid = function (gridName, pagerName) {
        var colNames = ['ID', 'Имя'];

        var columns = [];

        columns.push({
            name: 'ID',
            index: 'ID',
            hidden: true,
            sorttype: 'int',
            key: true,
            editable: false,
            editoptions: {
                readonly: true
            }
        });

        columns.push({
            name: 'Name',
            index: 'Name',
            width: 300,
            editable: true,
            edittype: 'text',
            editoptions: {
                size: 30,
                maxlength: 50
            },
            editrules: {
                required: true
            }
        });

        $(gridName).jqGrid({
            url: '/AdminHome/Tags',
            datatype: 'json',
            mtype: 'GET',
            height: 'auto',
            toppager: true,
            colNames: colNames,
            colModel: columns,
            pager: pagerName,
            rownumbers: true,
            rownumWidth: 40,
            rowNum: 500,
            sortname: 'Name',
            loadonce: true,
            jsonReader: {
                repeatitems: false
            }
        });

        var editOptions = {
            url: '/AdminHome/EditTag',
            editCaption: 'Редактировать тег',
            processData: "Сохранение...",
            closeAfterEdit: true,
            closeOnEscape: true,
            width: 400,
            afterSubmit: function (response, ArticleData) {
                var json = $.parseJSON(response.responseText);

                if (json) {
                    $(gridName).jqGrid('setGridParam', { datatype: 'json' });
                    return [json.success, json.message, json.id];
                }

                return [false, "Получить результат от сервера не удалось.", null];
            }
        };

        var addOptions = {
            url: '/AdminHome/AddTag',
            addCaption: 'Добавить тег',
            processData: "Сохранение...",
            closeAfterAdd: true,
            closeOnEscape: true,
            width: 400,
            afterSubmit: function (response, ArticleData) {
                var json = $.parseJSON(response.responseText);

                if (json) {
                    $(gridName).jqGrid('setGridParam', { datatype: 'json' });
                    return [json.success, json.message, json.id];
                }

                return [false, "Получить результат от сервера не удалось.", null];
            }
        };

        var deleteOptions = {
            url: '/AdminHome/DeleteTag',
            caption: 'Удалить тег',
            processData: "Сохранение...",
            width: 500,
            msg: "Удалить тег?",
            closeOnEscape: true,
            afterSubmit: NewsDotNet.GridManager.afterSubmitHandler
        };

        // configuring the navigation toolbar.
        $(gridName).jqGrid('navGrid', pagerName, {
            cloneToTop: true,
            search: false
        },

        editOptions, addOptions, deleteOptions);
    };

    NewsDotNet.GridManager.afterSubmitHandler = function (response, ArticleData) {

        var json = $.parseJSON(response.responseText);

        if (json) return [json.success, json.message, json.id];

        return [false, "Получить результат от сервера не удалось.", null];
    };

    $("#tabs").tabs({
        show: function (event, ui) {

            if (!ui.tab.isLoaded) {

                var gdMgr = NewsDotNet.GridManager,
                        fn, gridName, pagerName;

                switch (ui.index) {
                    case 0:
                        fn = gdMgr.articlesGrid;
                        gridName = "#tableArticles";
                        pagerName = "#pagerArticles";
                        break;
                    case 1:
                        fn = gdMgr.tagsGrid;
                        gridName = "#tableTags";
                        pagerName = "#pagerTags";
                        break;
                };

                fn(gridName, pagerName);
                ui.tab.isLoaded = true;
            }
        }
    });
});
