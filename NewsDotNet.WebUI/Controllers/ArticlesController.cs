﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewsDotNet.DomainModel.Entities;
using NewsDotNet.DomainModel.Abstract;
using NewsDotNet.WebUI.Models;


namespace NewsDotNet.WebUI.Controllers
{
    public class ArticlesController : Controller
    {
        private IArticlesRepository _articleRepo;
        private ITagsRepository _tagsRepo;

        public ArticlesController(IArticlesRepository articleRepo, ITagsRepository tagRepo)
        {
            _articleRepo = articleRepo;
            _tagsRepo = tagRepo;

        }
        
        public ActionResult Show(string addressName)
        {
            var article = _articleRepo.GetByAddressName(addressName);

            return View("Article", article);
        }
       
     public ActionResult Archive(string search){
            var articles = _articleRepo.All()
                .Where(a => a.State == ArticleStates.Published)
                .OrderByDescending(a => a.CreatedTime)
                .GroupBy(a => a.CreatedTime.ToShortDateString())
                .Select(group => new ArchiveArticles { Date = group.Key, Articles = group.ToList() });
              
      
        
            return View("ArchiveView",articles);
        }
        public ActionResult ArticlesWithTag(int tagId) 
        {
             var tag = _tagsRepo.GetById(tagId);
    IEnumerable<Article> articles;
    if(tag == null)
    {
      articles = new List<Article>();
    }
    else
    {
       articles = tag.Articles.Where(a => a.State == ArticleStates.Published).ToList();
     }
    return View("List", articles);
          
        }
        public ActionResult ArticlesForSearch(string search)
        {

           var articles = _articleRepo.All().Where(a => a.Title.ToLower().Contains(search.ToLower()) || a.Tags.Any(t => t.Name.ToLower().Contains(search.ToLower())) || a.Body.ToLower().Contains(search.ToLower()) || a.CreatedTime.ToString().Contains(search) || a.LastChangedTime.ToString().Contains(search));
           articles = articles.Where(a => a.State == ArticleStates.Published).ToList();
           return View("List", articles);

        }
      
	}
}