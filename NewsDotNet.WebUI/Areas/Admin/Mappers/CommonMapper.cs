﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using NewsDotNet.DomainModel.Abstract;
using NewsDotNet.DomainModel.Entities;
using NewsDotNet.WebUI.Areas.Admin.Models;

namespace NewsDotNet.WebUI.Areas.Admin.Mappers
{
    public class CommonMapper : IMapper
    {
        static CommonMapper()
        {
            Mapper.CreateMap<Article, ArticleViewModel>();
            Mapper.CreateMap<ArticleViewModel, Article>();
        }

        public TDestinationType Map<TSourceType, TDestinationType>(TSourceType source)
        {
            return Mapper.Map<TDestinationType>(source);
        }
    }
}