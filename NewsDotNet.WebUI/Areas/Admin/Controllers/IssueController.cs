﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewsDotNet.DomainModel.Entities;
using NewsDotNet.DomainModel.Abstract;

namespace NewsDotNet.WebUI.Areas.Admin.Controllers
{
    public class IssueController : Controller
    {
        IMainPageEntitiesRepository _mainPageRepo;
        IArticlesRepository _articlesRepo;

        public IssueController(IMainPageEntitiesRepository mainPageRepo, IArticlesRepository articlesRepo)
        {
            _mainPageRepo = mainPageRepo;
            _articlesRepo = articlesRepo;
        }
        //
        // GET: /Admin/Issue/
        public ActionResult Index()
        {
            return View(_mainPageRepo.All().Where(e => !e.IsFeatured).ToList());
        }


        [HttpPost]
        public ActionResult Save(IEnumerable<MainPageEntity> newEntities)
        {
            _mainPageRepo.Clear();
            if (newEntities != null)
            {
                _mainPageRepo.Add(newEntities); 
            }
            return new JsonResult { Data = new { result = "OK" } };
        }
	}
}