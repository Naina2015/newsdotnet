﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewsDotNet.DomainModel.Abstract;
using NewsDotNet.DomainModel.Entities;
using Newtonsoft.Json;
using NewsDotNet.WebUI.Models;

namespace NewsDotNet.WebUI.Areas.Admin.Controllers
{
    public class AdArticlesController : Controller
    {
        private IArticlesRepository _articlesRepo;
        
        public AdArticlesController(IArticlesRepository articlesRepo)
        {
            _articlesRepo = articlesRepo;
        }

        public ActionResult List(IEnumerable<int> articleIds)
        {
            //seletct only articles with ids form the list
            var result = _articlesRepo.All().Where(article => articleIds.Any(id => id == article.Id));

            var jsonSettings = new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };

            var convertedResult = JsonConvert.SerializeObject(result, jsonSettings);

            return Content(convertedResult, "application/json");
        }

        public ActionResult PagingList(int page = 1, int itemsPerPage = 10)
        {
            var articles = _articlesRepo.All().Skip((page - 1) * itemsPerPage).Take(itemsPerPage);
            var pagingInfo = new PagingInfo{
                CurrentPage = page,
                ItemsPerPage = itemsPerPage,
                TotalItems = _articlesRepo.All().Count()
            };

            var result = new ArticleListViewModel{
                Articles = articles,
                PagingInfo = pagingInfo
            };

            var jsonSettings = new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };

            var convertedResult = JsonConvert.SerializeObject(result, jsonSettings);
            return Content(convertedResult, "application/json");            
        }

	}
}