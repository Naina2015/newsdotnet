﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewsDotNet.DomainModel.Entities;
using NewsDotNet.DomainModel.Abstract;
using NewsDotNet.WebUI.Areas.Admin.Mappers;
using NewsDotNet.WebUI.Areas.Admin.Models;
using Ninject;
using System.Text;
using System.IO;

namespace NewsDotNet.WebUI.Areas.Admin.Controllers
{
    public class AdminArticlesController : Controller
    {
        private IArticlesRepository _articleRepo;
        private ITagsRepository _tagsRepo;
        private IMapper _modelMapper;

        public AdminArticlesController(IArticlesRepository articleRepo, ITagsRepository tagsRepo, IMapper modelMapper)
        {
            _articleRepo = articleRepo;
            _tagsRepo = tagsRepo;
            _modelMapper = modelMapper;
        }

        [HttpGet]
        public ActionResult New()
        {
            var article = new ArticleViewModel();
            ViewBag.Action = ActionType.NewArticle;
            return View("ArticleEditor", article);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult New(ArticleViewModel newArticleView, HttpPostedFileBase titleImage)
        {
            CheckAddressNameAvailability(newArticleView.AddressName, newArticleView.Id);
            if (null == titleImage)
                ModelState.AddModelError("TitleImagePath", "Прикрепите к статье изображение");
            if (ModelState.IsValid)
            {
                var article = _modelMapper.Map<ArticleViewModel, Article>(newArticleView);
                article.CreatedTime = DateTime.Now;
                article.LastChangedTime = DateTime.Now;
                article.Tags = TagStringToList(newArticleView.TagsString);
                SaveUploadedImage(article, titleImage);
                _articleRepo.Add(article);
                return RedirectToAction("Manage", "AdminHome");
            }
            ViewBag.Action = ActionType.NewArticle;
            return View("ArticleEditor", newArticleView);
        }

        [HttpGet]
        public ActionResult Edit(string addressName)
        {
            var article = _articleRepo.GetByAddressName(addressName);
            if (null == article)
                return HttpNotFound();
            var articleView = _modelMapper.Map<Article, ArticleViewModel>(article);
            articleView.TagsString = TagListToString(article.Tags);
            ViewBag.Action = ActionType.EditArticle;
            return View("ArticleEditor", articleView);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ArticleViewModel articleView, HttpPostedFileBase titleImage)
        {
            CheckAddressNameAvailability(articleView.AddressName, articleView.Id);
            if (ModelState.IsValid)
            {
                var article = _modelMapper.Map<ArticleViewModel, Article>(articleView);
                article.Tags = TagStringToList(articleView.TagsString);
                article.LastChangedTime = DateTime.Now;
                SaveUploadedImage(article, titleImage);
                _articleRepo.Update(article);
                return RedirectToAction("Manage", "AdminHome");
            }
            ViewBag.Action = ActionType.EditArticle;
            return View("ArticleEditor", articleView);
        }

        private string TagListToString(ICollection<Tag> tags)
        {
            if (tags.Count == 0)
                return String.Empty;
            StringBuilder builder = new StringBuilder();
            foreach(var tag in tags)
            {
                builder.AppendFormat("{0}, ", tag.Name);
            }
            builder.Remove(builder.Length - 2, 2);
            return builder.ToString();
        }

        private ICollection<Tag> TagStringToList(string tagString)
        {
            List<Tag> tags = new List<Tag>();
            if (!String.IsNullOrWhiteSpace(tagString))
            {
                var tagArray = tagString.Split(',');
                foreach (var s in tagArray)
                {
                    if (!String.IsNullOrWhiteSpace(s))
                    {
                        string name = s.Trim();
                        Tag t = _tagsRepo.GetByName(name);
                        if (null == t)
                            t = _tagsRepo.Add(new Tag { Name = name });
                        tags.Add(t);
                    }
                }
            }
            return tags;
        }

        private void SaveUploadedImage(Article article, HttpPostedFileBase image)
        {
            string newPath;
            string oldPath = article.TitleImagePath;
            bool pathChanged = !isOldImagePathRelevant(oldPath, article.AddressName);
            FileInfo oldImageInfo = null;
            if (pathChanged)
                oldImageInfo = new FileInfo(Server.MapPath(oldPath));
            if (null == image)
            {
                if (pathChanged)
                {   //there is an old image and article address changed => rename old image
                    newPath = String.Format("~/Content/Images/Articles/{0}{1}",
                                        article.AddressName,
                                        oldImageInfo.Extension);
                    oldImageInfo.MoveTo(newPath);
                }
                else
                    return;
            }
            else
            {   //there is a new image => save new image
                FileInfo newImageInfo = new FileInfo(image.FileName);
                newPath = String.Format("~/Content/Images/Articles/{0}{1}",
                                            article.AddressName,
                                            newImageInfo.Extension);
                image.SaveAs(Server.MapPath(newPath));

                if (pathChanged)
                {   //path changed and there is an old image => delete old image
                    oldImageInfo.Delete();
                }
            }
            article.TitleImagePath = newPath;
        }

        private void CheckAddressNameAvailability(string addressName, int id)
        {
            var existingArticle = _articleRepo.GetByAddressName(addressName);
            if (null != existingArticle && existingArticle.Id != id)
                ModelState.AddModelError("addressName", "Данный адрес уже принадлежит другой статье");
        }

        private bool isOldImagePathRelevant(string imagePath, string articleAddressName)
        {
            if (null == imagePath)
                return true;
            FileInfo info = new FileInfo(imagePath);
            string name = info.Name.Remove(info.Name.LastIndexOf('.'));
            return name.Equals(articleAddressName);
        }
    }

    public enum ActionType
    {
        NewArticle,
        EditArticle
    }
}