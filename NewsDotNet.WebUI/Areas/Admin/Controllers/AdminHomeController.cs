﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewsDotNet.DomainModel.Entities;
using NewsDotNet.DomainModel.Abstract;
using Newtonsoft.Json;
using System.Text;
using NewsDotNet.WebUI.Areas.Admin.Models;


namespace NewsDotNet.WebUI.Areas.Admin.Controllers
{
    public class AdminHomeController : Controller
    {
        private IArticlesRepository _articleRepo;
        private ITagsRepository _tagsRepo;


        public AdminHomeController(IArticlesRepository articleRepo, ITagsRepository tagsRepo)
        {
            _articleRepo = articleRepo;
            _tagsRepo = tagsRepo;
        }


        //
        // GET: /Admin/Home/
        public ActionResult Index()
        {
            return View(_articleRepo.All());
        }

        /// <summary>
        /// Return page to manage Articles and tags.
        /// </summary>
        public ActionResult Manage()
        {
            return View();
        }
         public ActionResult Deleted()
        {
            var articles = _articleRepo.All().Where(a => a.State == ArticleStates.Deleted).ToList();
           return View("DeletedArticles", articles);

       }
        #region Tags

        /// <summary>
        /// Return all the tags as JSON.
        /// </summary>
        /// <returns></returns>
        public ContentResult Tags()
        {
            var tags = _tagsRepo.All().ToList();

            return Content(JsonConvert.SerializeObject(new
            {
                page = 1,
                records = tags.Count(),
                rows = tags,
                total = 1
            }), "application/json");
        }

        /// <summary>
        /// Add a new tag.
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        [HttpPost]
        public ContentResult AddTag([Bind(Exclude = "ID")]Tag tag)
        {
            string json;

            var addedTag = _tagsRepo.Add(tag);

            if (null != addedTag)
            {
                json = JsonConvert.SerializeObject(new
                {
                    id = addedTag.ID,
                    success = true,
                    message = "Tag added successfully."
                });
            }
            else
            {
                json = JsonConvert.SerializeObject(new
                {
                    id = 0,
                    success = false,
                    message = "Тег с таким именем уже существует"
                });
            }

            return Content(json, "application/json");
        }

        /// <summary>
        /// Edit an existing tag.
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        [HttpPost]
        public ContentResult EditTag(Tag tag)
        {
            string json;

            if (ModelState.IsValid)
            {
                _tagsRepo.Update(tag);
                json = JsonConvert.SerializeObject(new
                {
                    id = tag.ID,
                    success = true,
                    message = "Changes saved successfully."
                });
            }
            else
            {
                json = JsonConvert.SerializeObject(new
                {
                    id = 0,
                    success = false,
                    message = "Failed to save the changes."
                });
            }

            return Content(json, "application/json");
        }

        /// <summary>
        /// Delete an existing tag.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ContentResult DeleteTag(int id)
        {
            _tagsRepo.Delete(id);

            var json = JsonConvert.SerializeObject(new
            {
                success = true,
                message = "Tag deleted successfully."
            });

            return Content(json, "application/json");
        }
        
        #endregion

        #region Articles

        public ContentResult Articles(JqInViewModel jqParams)
        {
            var articles = _articleRepo.GetPage(jqParams.page-1, jqParams.rows, jqParams.sidx, jqParams.sord == "asc");
            var totalArticles = _articleRepo.All().Count();
            return Content(JsonConvert.SerializeObject(new
            {
                page = jqParams.page,
                records = totalArticles,
                rows = articles,
                total = Math.Ceiling(Convert.ToDouble(totalArticles) / jqParams.rows)
            }, new CustomDateTimeConverter()), "application/json");
        }
        
        /// <summary>
        /// Delete an existing Article.
        /// </summary>
        [HttpPost]
        public ContentResult DeleteArticle(int id)
        {
            _articleRepo.Delete(id);

            var json = JsonConvert.SerializeObject(new
            {
                success = true,
                message = "Article deleted successfully."
            });

            return Content(json, "application/json");
        }
        #endregion

        [HttpPost]
        public JsonResult ArticleList()
        {
            var result = _articleRepo.All().ToList();

            return Json(result);
        }
	}
}